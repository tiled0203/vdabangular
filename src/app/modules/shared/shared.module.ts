import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {SearchComponent} from './search/search.component';
import {MediaComponent} from './media/media.component';


@NgModule({
  declarations: [SearchComponent, MediaComponent],
  imports: [
    CommonModule, FormsModule
  ],
  exports: [CommonModule, FormsModule, MediaComponent, SearchComponent]
})
export class SharedModule {
}
