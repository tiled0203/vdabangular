import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Movie} from '../../../model/Movie';
import {NgModel} from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  titleSubject: Subject<string> = new Subject();
  @Output()
  private searchTitle: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit(): void {
    this.titleSubject.pipe(debounceTime(1000), distinctUntilChanged())
      .subscribe(x => this.searchTitle.emit(x));
  }

  lookup(model: NgModel): void {
    console.log(model.control.errors);
    if (model.control.errors === null) {
      this.titleSubject.next(model.value);
    }
  }
}
