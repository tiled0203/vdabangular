import {Component, Input, OnInit} from '@angular/core';
import {Movie} from '../../../model/Movie';
import {TvShow} from '../../../model/TvShow';

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.css']
})
export class MediaComponent implements OnInit {

  @Input()
  media: Movie | TvShow;

  constructor() { }

  ngOnInit(): void {
  }

}
