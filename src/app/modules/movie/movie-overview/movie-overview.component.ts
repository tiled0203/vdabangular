import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {Movie} from '../../../model/Movie';
import {MovieService} from '../movie.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {truncate} from 'fs';

@Component({
  selector: 'app-movie-overview',
  templateUrl: './movie-overview.component.html',
  styleUrls: ['./movie-overview.component.css']
})
export class MovieOverviewComponent implements OnInit, OnDestroy {
  title = 'Movie list!';
  movies: Observable<Movie[]>;

  isOnline = true;
  isPublic = true;

  constructor(private movieService: MovieService, private route: Router) {
  }

  ngOnInit(): void {
    console.log('create');
    this.getMovies();
  }

  ngOnDestroy(): void {
    console.log('destroy');
  }

  getMovies(): void {
    this.movies = this.movieService.getMovies();
  }

  deleteMovie(movie: Movie): void {
    this.movieService.delete(movie).subscribe(() => this.getMovies());
  }

  showDetail(movie: Movie): void {
    this.route.navigate(['/movies', movie.id]);
  }

  lookupMovie(title: string): void {
    this.movies = this.movieService.searchMovie(title, this.isOnline, this.isPublic);
  }
}
