import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MovieAddComponent} from './movie-add/movie-add.component';
import {MovieDetailComponent} from './movie-detail/movie-detail.component';
import {MovieOverviewComponent} from './movie-overview/movie-overview.component';
import {SearchComponent} from '../shared/search/search.component';
import {FormsModule} from '@angular/forms';
import {AppModule} from '../../app.module';
import {MovieService} from './movie.service';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [MovieAddComponent, MovieDetailComponent, MovieOverviewComponent],
  imports: [
   SharedModule
  ],
  providers: [MovieService]
})
export class MovieModule {
}
