import {Component, OnDestroy, OnInit} from '@angular/core';
import {Form, FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {MovieService} from '../movie.service';
import {Movie} from '../../../model/Movie';
import {ForbiddenNameValidator} from '../../../components/validators/forbiddenTitleValidator';
import {Router} from '@angular/router';
import {Observable, range, Subject, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map, scan} from 'rxjs/operators';

@Component({
  selector: 'app-movie-add',
  templateUrl: './movie-add.component.html',
  styleUrls: ['./movie-add.component.css']
})
export class MovieAddComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  movieObservable: Observable<Movie[]>;
  isPublic = true;

  constructor(private movieService: MovieService, private formBuilder: FormBuilder, private route: Router) {
  }

  ngOnInit(): void {
    this.subscription = this.movieService.observable.subscribe();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  addThisMovie(movie: Movie): void {
    if (movie.id) {
      this.route.navigate(['/movies', movie.id]);
    } else {
      this.movieService.addMovie(movie.onlineId, this.isPublic).subscribe(movieResponse => this.route.navigate(['/movies', movieResponse.id]));
    }
  }


  lookupMovie(title: string): void {
    this.movieObservable = this.movieService.searchMovie(title, true, this.isPublic);
  }
}
