import {Injectable} from '@angular/core';
import {Movie} from '../../model/Movie';
import {HttpClient, HttpParams} from '@angular/common/http';
import {interval, Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {on} from 'cluster';

@Injectable({
  providedIn: 'root'
})
export class MovieService {


  observable: Observable<string> = new Observable<string>((observer) => {
    setInterval(() => {
      observer.next('default value ' + Date.now());
    }, 2000);
  });

  constructor(private http: HttpClient) {
  }

  getMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(`${environment.apiUrl}/movies`);
  }

  getMovieById(id: number): Observable<Movie> {
    return this.http.get<Movie>(`${environment.apiUrl}/movies/${id}`);
  }

  searchMovie(title: string, isOnline: boolean, isPublic: boolean): Observable<Movie[]> {
    return this.http.get<Movie[]>(`${environment.apiUrl}/movies` + '/search', {
      params: new HttpParams()
        .set('title', title)
        .set('public', String(isPublic))
        .set('online', String(isOnline))
    });
  }

  delete(movie: Movie): Observable<any> {
    return this.http.delete(`${environment.apiUrl}/movies/${movie.id}`);
  }

  addMovie(onlineId: string, isPublic: boolean): Observable<any> {
    return this.http.post(`${environment.apiUrl}/movies`, {apiId: onlineId}, {
      params: new HttpParams().set('public', String(isPublic))
    });
  }
}
