import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TvshowAddComponent } from './tvshow-add/tvshow-add.component';
import { TvshowDetailComponent } from './tvshow-detail/tvshow-detail.component';
import { TvshowOverviewComponent } from './tvshow-overview/tvshow-overview.component';
import {SharedModule} from '../shared/shared.module';



@NgModule({
  declarations: [TvshowAddComponent, TvshowDetailComponent, TvshowOverviewComponent],
  imports: [
    SharedModule
  ]
})
export class TvShowModule { }
