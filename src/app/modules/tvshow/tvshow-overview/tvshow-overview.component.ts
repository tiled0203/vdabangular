import {Component, OnInit} from '@angular/core';
import {TvShow} from '../../../model/TvShow';
import {Observable} from 'rxjs';
import {TvshowService} from '../tvshow.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tvshow-overview',
  templateUrl: './tvshow-overview.component.html',
  styleUrls: ['./tvshow-overview.component.css']
})
export class TvshowOverviewComponent implements OnInit {
  tvshows: Observable<TvShow[]>;
  isOnline = true;
  isPublic = true;

  constructor(private tvShowsService: TvshowService, private route: Router) {
  }

  ngOnInit(): void {
    this.getShows();
  }

  getShows(): void {
    this.tvshows = this.tvShowsService.getTvShows();
  }

  showDetail(show: TvShow): void {
    this.route.navigate(['tvshows', show.id]);
  }

  deleteTvshow(show: TvShow): void {
    this.tvShowsService.deleteTvshow(show).subscribe(() => this.getShows());
  }

  lookupTvShow(title: string): void {
    this.tvshows = this.tvShowsService.searchTvShows(title, this.isOnline);
  }
}
