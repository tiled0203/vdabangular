import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {TvShow} from '../../../model/TvShow';
import {TvshowService} from '../tvshow.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tvshow-add',
  templateUrl: './tvshow-add.component.html',
  styleUrls: ['./tvshow-add.component.css']
})
export class TvshowAddComponent implements OnInit {
  isOnline = true;
  tvShowObservable: Observable<TvShow[]>;

  constructor(private tvShowsService: TvshowService, private route: Router) {
  }

  ngOnInit(): void {
  }

  lookupTvShow(title: string): void {
    this.tvShowObservable = this.tvShowsService.searchTvShows(title, this.isOnline);
  }


  addThisTvShow(tvshow: TvShow): void {
    if (tvshow.id) {
      this.route.navigate(['/tvshows', tvshow.id]);
    } else {
      this.tvShowsService.addTvShow(tvshow.onlineId).subscribe(tvshowResponse => this.route.navigate(['/tvshow', tvshowResponse.id]));
    }
  }
}
