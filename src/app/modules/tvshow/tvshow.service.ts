import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {TvShow} from '../../model/TvShow';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TvshowService {

  constructor(private http: HttpClient) {
  }

  getTvShows(): Observable<TvShow[]> {
    return this.http.get<TvShow[]>(`${environment.apiUrl}/tvshows`);
  }

  getTvShow(id: number): Observable<TvShow> {
    return this.http.get<TvShow>(`${environment.apiUrl}/tvshows/${id}`);
  }

  searchTvShows(title: string, online: boolean): Observable<TvShow[]> {
    return this.http.get<TvShow[]>(`${environment.apiUrl}/tvshows/search`, {
      params: new HttpParams().set('title', title).set('online', online.toString()),
    });
  }

  addTvShow(onlineId: number): Observable<TvShow> {
    const addObj = {
      apiId: onlineId,
    };

    return this.http.post<TvShow>(`${environment.apiUrl}/tvshows/watchlist`, addObj);
  }

  deleteTvshow(show: TvShow): Observable<TvShow> {
    return this.http.delete<TvShow>(`${environment.apiUrl}/tvshows/${show.id}`);
  }
}
