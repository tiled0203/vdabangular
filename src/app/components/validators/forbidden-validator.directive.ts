import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from '@angular/forms';
import {ForbiddenNameValidator} from './forbiddenTitleValidator';

@Directive({
  selector: '[appForbiddenValidatorDirective]',
  providers: [{provide: NG_VALIDATORS, useExisting: ForbiddenValidatorDirective, multi: true}]
})
export class ForbiddenValidatorDirective implements Validator {

  @Input() regexTitle: string;

  constructor() {
  }

  validate(control: AbstractControl): ValidationErrors | null {
    console.log('validator');
    if (this.regexTitle) {
      const forbiddenNameValidator = ForbiddenNameValidator(new RegExp(this.regexTitle));
      return forbiddenNameValidator(control);
    } else {
      return null;
    }
  }

}
