import {ValidateFn} from 'codelyzer/walkerFactory/walkerFn';
import {AbstractControl} from '@angular/forms';

export function ForbiddenNameValidator(regExp: RegExp): any {
  return (control: AbstractControl): { [key: string]: any } => {
    const forbidden = regExp.test(control.value);
    if (control.value === '') {
      return {errorMsg: 'Field is required!'};
    }
    if (forbidden) {
      return {errorMsg: control.value + ' is forbidden!'};
    } else {
      return null;
    }
  };

}
