import {Component, OnDestroy, OnInit} from '@angular/core';
import {MovieService} from '../../modules/movie/movie.service';
import {Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {ChatService} from '../../services/chat.service';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {

  constructor(private chatService: ChatService) {
  }

  messages: string[] = [];
  private subscription: Subscription;
  msg: string;

  ngOnInit(): void {
    this.subscription = this.chatService.behaviorSubject.pipe(map(value => value + ' ChatComponent'))
      .subscribe(value => this.messages.push(value));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  sendMsg(): void {
    this.chatService.behaviorSubject.next(this.msg);
  }
}
