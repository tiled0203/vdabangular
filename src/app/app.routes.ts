import {Route} from '@angular/router';
import {MovieOverviewComponent} from './modules/movie/movie-overview/movie-overview.component';
import {MovieDetailComponent} from './modules/movie/movie-detail/movie-detail.component';
import {MovieAddComponent} from './modules/movie/movie-add/movie-add.component';
import {ChatComponent} from './components/chat/chat.component';
import {TvshowOverviewComponent} from './modules/tvshow/tvshow-overview/tvshow-overview.component';
import {TvshowAddComponent} from './modules/tvshow/tvshow-add/tvshow-add.component';
import {TvshowDetailComponent} from './modules/tvshow/tvshow-detail/tvshow-detail.component';

export const routes: Route[] = [
  {path: 'movies', component: MovieOverviewComponent},
  {path: 'movies/add', component: MovieAddComponent},
  {path: 'movies/chat', component: ChatComponent},
  {path: 'movies/:id', component: MovieDetailComponent},
  {path: 'tvshows', component: TvshowOverviewComponent},
  {path: 'tvshows/add', component: TvshowAddComponent},
  {path: 'tvshows/:id', component: TvshowDetailComponent},
  {path: '**', redirectTo: 'movies', pathMatch: 'full'}
];
