import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headers = {
      setHeaders: {Authorization: '123456789'}
    };
    const httpRequest = req.clone(headers);
    console.log(httpRequest);

    // const observable = next.handle(httpRequest);
    // const started = Date.now();
    // observable.subscribe(value => {
    //   if (value instanceof HttpResponse) {
    //     const elapsed = Date.now() - started;
    //     console.log(`Request time was ${elapsed} ms.`);
    //   }
    // });
    return next.handle(httpRequest);
  }
}
