import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  behaviorSubject: BehaviorSubject<string> = new BehaviorSubject<string>('Welcome');

  constructor() { }
}
