import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {MovieOverviewComponent} from './modules/movie/movie-overview/movie-overview.component';
import {AppHeaderComponent} from './components/app-header/app-header.component';
import {RouterModule} from '@angular/router';
import {routes} from './app.routes';
import {MovieDetailComponent} from './modules/movie/movie-detail/movie-detail.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MovieAddComponent} from './modules/movie/movie-add/movie-add.component';
import {ForbiddenValidatorDirective} from './components/validators/forbidden-validator.directive';
import {ChatComponent} from './components/chat/chat.component';
import {SearchComponent} from './modules/shared/search/search.component';
import {InterceptorService} from './services/interceptor.service';
import {MovieModule} from './modules/movie/movie.module';
import {TvShowModule} from './modules/tvshow/tvshow.module';


@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    ForbiddenValidatorDirective,
    ChatComponent
  ],
  imports: [
    BrowserModule,
    TvShowModule,
    MovieModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule, ReactiveFormsModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useExisting: InterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
